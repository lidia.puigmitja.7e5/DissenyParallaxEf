using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    private SpriteRenderer _sr;
    private float _speedAtenuador = 0.3f;
    private float _speedAtenuadorVertical = 0.05f;



    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
    

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(_speedAtenuador * Input.GetAxis("Horizontal") + transform.position.x, _speedAtenuadorVertical * Input.GetAxis("Vertical") + transform.position.y, transform.position.z);

   


    }
}
