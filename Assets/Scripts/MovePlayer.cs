using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{

    private float _speed= 0.3f;
    private Rigidbody2D _rigidbody;

    private float _inputMovimientoX;
    public LayerMask capaFloor;
    private bool _muerto = false;
    private bool atake=false;
    private Animator _animator;
    private BoxCollider2D _boxCollider;
    private bool _lookRight = true;
    private float fuerza;
    private float _distanceRay = 0.2f;

    private SpawnerControl spawner;



    // Start is called before the first frame update
    void Start()
    {
        _speed = 5;
        _rigidbody = GetComponent<Rigidbody2D>();
        _boxCollider = GetComponent<BoxCollider2D>();
        fuerza = 5.50f;
        _animator = GetComponent<Animator>();

        spawner = GameObject.Find("Spawner").GetComponent<SpawnerControl>();


    }
    void Update()
    {
        if (_muerto == false)
        {
            ProcesarMovimiento();
            ProcesarSalto();
            GetAnimations();
            ProcesarAtake();
        }

    }

    bool OnTheFloor()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(_boxCollider.bounds.center, new Vector2(_boxCollider.bounds.size.x, _boxCollider.bounds.size.y), 0f, Vector2.down, _distanceRay, capaFloor);

        return raycastHit.collider != null;

    }

    void ProcesarSalto()
    {

        if ((Input.GetKeyDown(KeyCode.Space)) && OnTheFloor())
        {

            _rigidbody.AddForce(Vector2.up * fuerza, ForceMode2D.Impulse);


        }


    }

    void ProcesarAtake()
    {

        if ((Input.GetKeyDown(KeyCode.LeftControl)))
        {
            atake = true;
            _animator.SetBool("Attak", atake);

        }

    }

    void ProcesarMovimiento()
    {
        atake = false;
        _inputMovimientoX = Input.GetAxis("Horizontal");


        _rigidbody.velocity = new Vector2(_inputMovimientoX * _speed, _rigidbody.velocity.y);

        GestionarOrientacion();
    }

    void GestionarOrientacion()
    {
        if ((_lookRight == true && _inputMovimientoX < 0) || (_lookRight == false && _inputMovimientoX > 0))
        {
            _lookRight = !_lookRight;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }

    }

    void GetAnimations()
    {
        _animator.SetFloat("Jump", _rigidbody.velocity.y);
        _animator.SetFloat("MoveHorizontal", _inputMovimientoX);
        _animator.SetBool("Attak", atake);
    }


    public void PlayerDeath()
    { 
        spawner.enabled = false;
        _muerto = true;
        _animator.SetBool("Death", _muerto);
        Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
        Debug.Log("Me mori");
       
     

    }
}
