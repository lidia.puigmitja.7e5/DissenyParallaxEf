using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlFire : MonoBehaviour
{
    private float radio = 2f;
    private float fuerzaExplosion = 250f;


    private MovePlayer _player;
 

    // Start is called before the first frame update
    void Start()
    {

   
        _player = GameObject.Find("personatge").GetComponent<MovePlayer>();
      

            fuerzaExplosion = 250f;
     

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

      

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("me choqu� con el prota!");

            Destroy(gameObject);
            _player.PlayerDeath();


        }

        if (collision.gameObject.tag == "Floor")
        {
            Debug.Log("me choqu�!");
           
            Explosion();
            Collider2D collider = gameObject.GetComponent<Collider2D>();
            Rigidbody2D rigthbody = gameObject.GetComponent<Rigidbody2D>();
            rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;

            collider.isTrigger = true;
            Destroy(gameObject);

        }
    }



    // Update is called once per frame
    void Update()
    {
    }

    public void Explosion()
    {
        //circulo alrededor d la bomba, guardar objetos q toque en arreglo
        Collider2D[] objetos = Physics2D.OverlapCircleAll(transform.position, radio);
        foreach (Collider2D colisionador in objetos)
        {
            Rigidbody2D rb2d = colisionador.GetComponent<Rigidbody2D>();
            if (colisionador.tag == "Player")
            {
                if (rb2d != null)
                {
                    Vector2 direccion = colisionador.transform.position - transform.position;
                    float distancia = direccion.magnitude;
                    float fuerzafinal = fuerzaExplosion / distancia;
                    rb2d.AddForce(direccion * fuerzafinal);
                    Debug.Log(colisionador.transform.position);
                }
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radio);
    }

}
