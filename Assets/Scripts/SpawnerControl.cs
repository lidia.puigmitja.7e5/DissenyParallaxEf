using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControl : MonoBehaviour
{
  
    public GameObject SmallFirePrefab;

    float tiempo = 0;
    float intervalo = 0;
    float framesTotal = 6;

    private BoxCollider2D _colliderFloor;

    // Start is called before the first frame update
    void Start()
    {
        
        _colliderFloor = GameObject.Find("Floor").GetComponent<BoxCollider2D>();
      


    }

    // Update is called once per frame
    void Update()
    {
       
         tiempo += Time.deltaTime;
     
        if (tiempo >= 1 / framesTotal)
        {
            intervalo++;
            tiempo = 0;

            if (intervalo == 4)
            {
                Vector3 newPos = new Vector3(Random.Range(-(_colliderFloor.bounds.size.x / 2), _colliderFloor.bounds.size.x / 2), transform.position.y, transform.position.z);
                Instantiate(SmallFirePrefab, newPos, Quaternion.identity);
                intervalo = 0;
            }
        }

        
      

      

        
    }
}
